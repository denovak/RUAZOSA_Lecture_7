package hr.fer.tel.ruazosa.notes

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

/**
 * Created by dejannovak on 08/05/2018.
 */
@Entity
data class Note(
    @Id @GeneratedValue
    var id: Long = 0,
    var noteDate: Date? = null,
    var noteTitle: String? = null,
    var noteDescription: String? = null)
